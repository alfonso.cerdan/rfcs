<!---
PLEASE READ THIS

1. Please make sure your issue isn't a duplicate from an existing issue
2. Please name your issue title `Change: OLD_PATTERN_NAME to NEW_PATTERN_NAME`
3. If this is a two way door decision, please label it as ~"two way door decision"
--->

### Change pattern proposal: OLD_PATTERN_NAME to NEW_PATTERN_NAME

#### Old Pattern

(1-2 sentence summary of old pattern)

#### New Pattern

(1-2 sentence summary of new pattern)

### Advantages of switching patterns

1. (list out pro's)

### Disadvantages of switching patterns

1. (list out cons's)

### What is the impact on our existing codebase?

(1-2 sentence summary)
