<!---
PLEASE READ THIS

1. Please make sure your issue isn't a duplicate from an existing issue
2. Please name your issue title `New: NEW_PATTERN_NAME`
3. If this is a two way door decision, please label it as ~"two way door decision"
--->

### New Pattern Proposal: NEW_PATTERN_NAME

(1-2 sentence summary of pattern)

### Advantages of new pattern

1. (list out pro's)

### Disadvantages of new pattern

1. (list out cons's)

### What is the impact on our existing codebase?

(1-2 sentence summary)
