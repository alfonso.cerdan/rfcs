# RFCs for changes to GitLab Frontend

Would you like to propose a new or a change to an existing frontend pattern in our codebase? Please create an issue here so that we can discuss it using the RFC (Request for Comments) process.

## Why should we use this process?

We used to discuss change proposals in gitlab-ce issues but we have now scaled frontend to such a size in which it is no longer effective to discuss them in the gitlab-ce project. Our previous method was
- Difficult to surface historical conversations around specific frontend process decisions
- Difficult for the team to track what changes are being proposed and implemented

## Goals of this process
- Single Source of Truth (SSOT) to track all frontend process changes, so that it's easy for new team members and the wider GitLab community to understand certain decisions
- Create a straightforward format for the team to make, discuss and implement decisions regarding frontend process changes
- Ensure that there is a DRI (Directly Responsible Individual) so that proposals don't go stale

## How will this RFC process work?

See the progress of our RFCs in this [issue board](https://gitlab.com/gitlab-org/frontend/rfcs/-/boards/1184859)

1. Create an issue using the templates provided
1. Share the RFC issue on the frontend weekly call and on the #frontend slack channel
1. Give the frontend team some time to review the proposal and provide comments about the proposal
1. If there is a strong concensus towards a decision, and it is a [two-way door decision](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions),
    - label the issue as `will implement` and select a frontend team member to lead the implementation of the process and share this with the team
    - after the implementation is complete, change the label from `will implement` to `implemented` and close the issue
1. Otherwise, assign the `~needs-EM-decision` issue label. The Frontend Engineering Managers will review to make the final decision based on what is presented in the issue and comment on it with the decision and reasoning
    - If the decision is to proceed, we should follow the same steps as step 4.
    - If the decision is not to proceed, the issue should be closed. If there is a disagreement, we should still [disagree, commit and disagree](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree)
    - Before the implementation or closing the issue, the Frontend EMs will give Frontend maintainers a week to raise concerns.
